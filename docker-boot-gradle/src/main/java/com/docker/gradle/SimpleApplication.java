package com.docker.gradle;

import com.docker.gradle.controller.ToDo;
import com.docker.gradle.controller.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApplication implements CommandLineRunner {

    @Autowired
    private ToDoRepository toDoRepository;

    public static void main(String[] args) {
        SpringApplication.run(SimpleApplication.class, args);
    }

    public void run(String... args) throws Exception {
        ToDo toDo = new ToDo();
        toDo.setName("Test Task#1");
        toDo.setDescription("cnjdksbv duisgvi gviu esgviu sf");
        toDo.setCreatedBy("Roman Andriianov");
        toDoRepository.save(toDo);
    }
}
