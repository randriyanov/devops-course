1.curl
Вот как я например отправляю soap запрос

curl -X POST \
  http://localhost:8080/alfa/creditdecisioning/v2/externalcreditdecision \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 1677' \
  -H 'Content-Type: text/xml' \
  -H 'Host: localhost:8080' \
  -H 'Postman-Token: f1e09a45-b391-43cc-b86f-ca8639110020,86183312-4538-486c-a84f-78c184a3bb47' \
  -H 'User-Agent: PostmanRuntime/7.17.1' \
  -H 'cache-control: no-cache' \
  -d '<?xml version='\''1.0'\'' encoding='\''UTF-8'\''?>
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
	<S:Header>
		<ns6:context xmlns:asst="http://ws.chpconsulting.com/alfa/contract/v1" xmlns:core="http://www.chpconsulting.com/alfa/core" xmlns:credit="http://ws.chpconsulting.com/alfa/credit/v2" xmlns:ns6="http://ws.chpconsulting.com/alfa/requestcontext/v1" xmlns:ns7="http://ws.chpconsulting.com/alfa/creditdecisioning/v2/externalcreditdecision" xmlns:thir="http://ws.chpconsulting.com/alfa/thirdparty/v1">
			<ns6:realmId/>
			<ns6:segments/>
			<ns6:sessionId>node012hh9whdcowa912k4pu1h6m3et299</ns6:sessionId>
			<ns6:systemId>ALFA_POS</ns6:systemId>
			<ns6:userId>TESTUSERSALESPERSON1</ns6:userId>
		</ns6:context>
	</S:Header>
	<S:Body>
		<ns7:requestExternalCreditDecision xmlns:asst="http://ws.chpconsulting.com/alfa/contract/v1" xmlns:core="http://www.chpconsulting.com/alfa/core" xmlns:credit="http://ws.chpconsulting.com/alfa/credit/v2" xmlns:ns6="http://ws.chpconsulting.com/alfa/requestcontext/v1" xmlns:ns7="http://ws.chpconsulting.com/alfa/creditdecisioning/v2/externalcreditdecision" xmlns:thir="http://ws.chpconsulting.com/alfa/thirdparty/v1">
			<externalCreditDecisionRequest>
				<credit:creditDecisionEntity xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="credit:proposalCreditDecisionEntity">
					<credit:agreementNumber></credit:agreementNumber>
				</credit:creditDecisionEntity>
				<credit:creditDecisionId></credit:creditDecisionId>
				<credit:workflowCaseId></credit:workflowCaseId>
			</externalCreditDecisionRequest>
		</ns7:requestExternalCreditDecision>
	</S:Body>
</S:Envelope>'

curl -X GET \
  'https://api.ad.smaato.net/oapi/v6/ad?pub=1100041981&adspace=130547539&cs_profession=developer&format=display' \
  -H 'Postman-Token: 5d8176a2-fd88-4db3-bf80-3f9b8a803c31' \
  -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 9_0_2 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13A452 Safari/601.1 PTST/396' \
  -H 'X-FORWARDED-FOR: 8.8.8.8' \
  -H 'cache-control: no-cache'

2.telnet
telnet 127.0.0.0.1 8080

3.iostat
sudo apt-get install sysstat

http://www.dba-oracle.com/t_linux_iostat.htm
iostat показывает следующую информацию

Device	    The Device Name
Blk_read/s	The number of blocks read per second
Blk_wrtn/s	The number of blocks written per second
Blk_read	Total number of blocks read
Blk_wrtn	Total number of blocks written
