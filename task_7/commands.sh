aws s3 cp . s3://docker-swarm-elk --recursive
aws s3 cp s3://docker-swarm-elk/ elk-stack --recursive
aws s3 sync . s3://docker-swarm-elk
aws s3 sync s3://docker-swarm-elk/ elk-stack
for NODE in $(docker node ls --format '{{.Hostname}}'); do echo -e "${NODE} - $(docker node inspect --format '{{.Status.Addr}}' "${NODE}")"; done


docker node update 7jd21ktexjkbln4wnstgw0h9w --label-add worker=elastic
docker node update k2qsfphyqjzai66iowej65pjf --label-add worker=kibana
docker node update k2qsfphyqjzai66iowej65pjf --label-add worker=logstash

docker stack deploy -c docker-stack.yml elk_stack
docker swarm join-token manager

docker service ps --no-trunc elk_traefik