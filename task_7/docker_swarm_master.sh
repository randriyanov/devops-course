#!/bin/bash
sudo apt-get update
sudo apt-get install awscli
aws s3 cp s3://docker-swarm-elk/ elk-stack --recursive
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker ubuntu
docker swarm init