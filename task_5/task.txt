1. create new repo on bitbucket
2. git init
3. git remote add origin https://randriyanov@bitbucket.org/randriyanov/devops-course.git
4. git add .
5. git commit
6. git push -u origin master
7. git status
8. если нужно склонить git clone https://randriyanov@bitbucket.org/randriyanov/devops-course.git
9. создал .gitignore файл
10. git diff -> показать что поменялось
11. remove file
    touch test.txt
    git commit -m 'created test file'
    rm test.txt
    git status
    git rm test.txt
    git commit 'removed test.txt'
    git commit -m 'removed test.txt'
    git push
12. git mv переименовать
13. git checkout -b test_branch
    commit file test.txt
14. git checkout [branch_name] -> переключиться на ветку без ее создания
15. напечатать форматированый лог git log --pretty=format:"%h - %an, %ar : %s"
